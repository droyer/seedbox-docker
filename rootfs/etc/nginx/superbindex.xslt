<?xml version="1.0" encoding="UTF-8" ?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" encoding="UTF-8" />

  <xsl:template match="/">
    <xsl:text disable-output-escaping="yes">&lt;!DOCTYPE html&gt;</xsl:text>

    <html>
      <head>
        <title>Seedbox</title>

        <meta name="version" content="1.1.1" />
        <meta name="viewport" content="initial-scale=1, minimum-scale=1, shrink-to-fit=no, width=device-width" />

        <style><![CDATA[*{box-sizing:border-box}body{margin:3.5rem}html{background-color:#2d2d2d;color:#d3d0c8;font-family:Menlo, Monaco, Consolas, 'Courier New', monospace;font-size:16px}.list{display:block;list-style:none;margin:0;padding:0;height:100%}.list__highlight{background-color:#fc6;border-radius:2px;color:#2d2d2d}.list__item{display:block}.list__item--directory+.list__item--file{margin-top:1rem}.list__link{display:block;line-height:0.45;outline:0;padding-top:0.5rem;padding-bottom:calc(0.5rem + 0.125em);text-decoration:none;transition:color 0.025s linear}.list__link--directory{color:#69c}.list__link--file{color:#d3d0c8}.list__link--active{color:#c9c}.list__link--active>.list__highlight{background-color:#c9c}]]></style>
      </head>
      <body>
        <script id="data" type="application/json">
          <xsl:text>[</xsl:text>
          <xsl:for-each select="list/*">
            <xsl:if test="position() != 1">
              <xsl:text>, </xsl:text>
            </xsl:if>
            <xsl:text>{</xsl:text>
            <xsl:text>"name": "</xsl:text>
            <xsl:value-of select="." />
            <xsl:text>", "type": "</xsl:text>
            <xsl:value-of select="name(.)" />
            <xsl:text>"</xsl:text>
            <xsl:text>}</xsl:text>
          </xsl:for-each>
          <xsl:text>]</xsl:text>
        </script>

        <script><![CDATA[(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){exports.bindContext=function(context){var property;for(property in context){if(context[property]instanceof Function){context[property]=context[property].bind(context)}}};exports.extendProperties=function(context,properties){var property;for(property in properties){context[property]=properties[property]}}},{}],2:[function(require,module,exports){var helper=require("./helper");var Item=function(properties){helper.bindContext(this);this.defineProperties();this.initialize(properties);this.bindEvents()};Item.prototype={$item:undefined,$link:undefined,highlighted:false,activate:function(){this.active=true},bindEvents:function(){this.$link.addEventListener("focusin",this.activate);this.$link.addEventListener("focusout",this.deactivate);this.$link.addEventListener("mouseenter",this.activate);this.$link.addEventListener("mouseleave",this.deactivate)},deactivate:function(){this.active=false},defineProperties:function(){var active=false;var name;var type;Object.defineProperties(this,{active:{get:function(){return active},set:function(value){if(active==value){return}active=value;this.$link.classList.toggle("list__link--active",active)}},name:{get:function(){return name},set:function(value){if(name==value){return}name=value;this.$link.setAttribute("href",name);this.$link.textContent=name}},type:{get:function(){return type},set:function(value){if(type==value){return}type=value;this.$item.classList.add("list__item--"+type);this.$link.classList.add("list__link--"+type)}}});return this},highlight:function(letters){this.highlighted=letters.length!=0&&this.name.toLowerCase().indexOf(letters)==0;if(this.highlighted){var regExp=new RegExp("^("+letters+")","i");var template='<mark class="list__highlight">$1</mark>';this.$link.innerHTML=this.name.replace(regExp,template);this.$link.focus()}else{this.$link.textContent=this.name}},initialize:function(properties){this.$link=document.createElement("a");this.$link.classList.add("list__link");this.$item=document.createElement("li");this.$item.classList.add("list__item");this.$item.appendChild(this.$link);helper.extendProperties(this,properties);return this}};module.exports=Item},{"./helper":1}],3:[function(require,module,exports){var helper=require("./helper");var Item=require("./item");var List=function(properties){helper.bindContext(this);this.defineProperties();this.initialize(properties);this.bindEvents()};List.prototype={$list:undefined,whitelistedLetters:"",bindEvents:function(){document.body.addEventListener("keyup",this.watchType)},defineProperties:function(){var list;Object.defineProperties(this,{list:{get:function(){return list},set:function(value){list=value.map(this.setup)}}});return this},filter:function(letter){this.whitelistedLetters+=letter;this.updateList()},initialize:function(properties){this.$list=document.createElement("ol");this.$list.classList.add("list");helper.extendProperties(this,properties);return this},setup:function(item){item=new Item(item);this.$list.appendChild(item.$item);return item},unfilter:function(){this.whitelistedLetters="";this.updateList()},updateItem:function(item){item.highlight(this.whitelistedLetters)},updateList:function(){this.list.forEach(this.updateItem)},watchType:function(event){var key=event.key.toLowerCase();if(key=="escape"){return this.unfilter()}if(key.length!=1||/[^a-z0-9]/.test(key)){return}this.filter(key)}};module.exports=List},{"./helper":1,"./item":2}],4:[function(require,module,exports){var helper=require("./helper");var List=require("./list");var main=function(){var list=new List({list:JSON.parse(document.getElementById("data").textContent)});document.body.appendChild(list.$list)};document.addEventListener("DOMContentLoaded",main)},{"./helper":1,"./list":3}]},{},[4]);]]></script>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
