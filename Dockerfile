FROM debian:buster-slim

ARG SEEDBOX_URL=https://gitlab.com/droyer/SeedboxApp/uploads/5d31bd935586306f19a3f308d6e9fcad/seedbox.tar.gz
ARG RTORRENT_VER=0.9.8
ARG LIBTORRENT_VER=0.13.8

WORKDIR /usr/seedbox

RUN apt-get update && apt-get upgrade -y \
 && apt-get install -y \
    apt-transport-https \
    gnupg2 \
    wget \
    git \
    psmisc \
    python \
    screen \
    nginx \
    automake \
    build-essential \
    libcppunit-dev \
    libncurses5-dev \
    libtool \
    pkg-config \
    zlib1g-dev \
    libssl-dev \
    libcurl4-openssl-dev \
 && cd /usr \
 && wget -qO- ${SEEDBOX_URL} | tar xz \
 && cd /tmp \
 && git clone https://github.com/mirror/xmlrpc-c.git \
 && git clone https://github.com/rakshasa/libtorrent.git \
 && git clone https://github.com/rakshasa/rtorrent.git \
 && cd xmlrpc-c/stable && ./configure && make -j && make install \
 && cd /tmp/libtorrent && git checkout v${LIBTORRENT_VER} && ./autogen.sh && ./configure && make -j && make install \
 && cd /tmp/rtorrent && git checkout v${RTORRENT_VER} && ./autogen.sh && ./configure --with-xmlrpc-c && make -j && make install && ldconfig \
 && wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.asc.gpg \
 && mv microsoft.asc.gpg /etc/apt/trusted.gpg.d/ \
 && wget -q https://packages.microsoft.com/config/debian/10/prod.list \
 && mv prod.list /etc/apt/sources.list.d/microsoft-prod.list \
 && chown root:root /etc/apt/trusted.gpg.d/microsoft.asc.gpg \
 && chown root:root /etc/apt/sources.list.d/microsoft-prod.list \
 && apt-get update && apt-get install -y aspnetcore-runtime-3.1 \
 && rm /etc/nginx/sites-enabled/* \
 && rm -rf /tmp/*

COPY rootfs /

EXPOSE 8080

CMD ["run.sh"]
